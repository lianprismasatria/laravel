@extends('utama')

@section('title')
    <h3>Daftar Pemeran</h3>
@endsection

@section('isi')
<a href="/cast/create" class="btn btn-primary" >Tambah Data baru</a>

<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">id</th>
        <th scope="col">Nama</th>
        <th scope="col">Bio</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$item)
        <tr>
            <td>{{$key +1}}</td>
            <td>{{$item->name}}</td>
            <td>{{$item->bio}}</td>
            <td>
                
                <form action="/cast/{{$item->id}}" method="POST">
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-primary btn-sm" >Edit</a>
                    <a href="/cast/{{$item->id}}" class="btn btn-success btn-sm" >Detail</a>
                @method("delete")
                @csrf
                <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
                
            </td>
        </tr>
        @empty
        <tr>
            <td>Data kosong</td>
        </tr>
        @endforelse      
    </tbody>
  </table>
    
@endsection