@extends('utama')

@section('title')
    

<h2>List Game</h2>
@endsection

@section('isi')
    

<a href="/game/create" class="btn btn-primary mb-2">Tambah</a>



<table class="table">

<thead class="thead-light">

<tr>

<th scope="col">#</th>

<th scope="col">Name</th>

<th scope="col">Gameplay</th>

<th scope="col">Developer</th>

<th scope="col">Year</th>

<th scope="col" >Actions</th>

</tr>

</thead>

<tbody>

    @forelse ($game as $key=>$item)
    <tr>
        <td>{{$key +1}}</td>
        <td>{{$item->name}}</td>
        <td>{{$item->gameplay}}</td>
        <td>{{$item->developer}}</td>
        <td>{{$item->year}}</td>
        <td>
            
            <form action="/game/{{$item->id}}" method="POST">
                <a href="/game/{{$item->id}}/edit" class="btn btn-primary btn-sm" >Edit</a>
                <a href="/game/{{$item->id}}" class="btn btn-success btn-sm" >Detail</a>
            @method("delete")
            @csrf
            <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </form>
            
        </td>
    </tr>
    @empty
    <tr>
        <td>Data kosong</td>
    </tr>
    @endforelse
</tbody>

</table>


@endsection
