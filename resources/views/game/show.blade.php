@extends('utama')

@section('title')
    

<h2>Detail Data Game</h2>
@endsection

@section('isi')

<h4>Nama Game : {{$game->name}}</h4>
<p>Gameplay : {{$game->gameplay}}</p>
<h4>Pengembang : {{$game->developer}}</h4>
<h4>Tahun Rilis : {{$game->year}}</h4>

@endsection