@extends('utama')

@section('title')
    

<h2>Edit Data Game</h2>
@endsection

@section('isi')
    

<form action="/game/{{$game->id}}" method="POST">
    @csrf
    @method("PUT")
    <div class="form-group">
      <label for="exampleInputEmail1">Nama game</label>
      <input type="text" name="name"class="form-control"  placeholder="Nama game">
    </div>
    @error('name')
    <div class="alert alert-danger">{{$message}}  </div>
    @enderror
    <div class="form-group">
      <label>Gameplay</label>
    <textarea name="gameplay" cols="50" rows="3"></textarea>    
    </div>
    @error('gameplay')
    <div class="alert alert-danger">{{$message}}  </div>
    @enderror
    <div class="form-group">
        <label for="exampleInputEmail1">Developer</label>
        <input type="text" name="developer"class="form-control"  placeholder="Pengembang game">
      </div>
      @error('developer')
      <div class="alert alert-danger">{{$message}}  </div>
      @enderror
      <div class="form-group">
        <label for="exampleInputEmail1">Tahun</label>
        <input type="text" name="year"class="form-control"  placeholder="Tahun rilis">
      </div>
      @error('year')
      <div class="alert alert-danger">{{$message}}  </div>
      @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection
