@extends('utama')

@section('title')
<h3>Masukkan nama pemeran</h3>
@endsection

@section('isi')

<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label for="exampleInputEmail1">Masukkan Nama Pemeran</label>
      <input type="text" name="name"class="form-control"  placeholder="Nama">
    </div>
    @error('name')
    <div class="alert alert-danger">{{$message}}  </div>
    @enderror
    <div class="form-group">
      <label>Bio</label>
    <textarea name="bio" cols="30" rows="10"></textarea>    
    </div>
    @error('bio')
    <div class="alert alert-danger">{{$message}}  </div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
    
@endsection