<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@daftar');

Route::get ('/table', function (){
    return view ('table');

});

Route::get ('/data-table', function (){
    return view ('data-tables');

});

Route::get('/cast/create', 'CastController@create');

Route::post('/cast', 'CastController@store');

Route::get('/cast', 'CastController@index');

Route::get('/cast/{cast_id}', 'CastController@show');

Route::get('/cast/{cast_id}/edit', 'CastController@edit');

Route::put('/cast/{cast_id}' , 'CastController@update');

Route::delete('/cast/{cast_id}' , 'CastController@destroy');

Route::get('/game/create', 'GameController@create');
Route::post('/game','GameController@save');
Route::get('/game','GameController@index');
Route::get('/game/{game_id}', 'GameController@show');
Route::get('/game/{game_id}/edit', 'GameController@edit');
Route::put('/game/{game_id}' , 'GameController@update');
Route::delete('/game/{game_id}' , 'GameController@destroy');
