<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create(){
        return view('cast');
    }

    public function store(Request $request){
        $request->validate([
            'name'=>'required',
            'bio'=>'required',

        ]);

        DB::table('cast')->insert([
            'name'=>$request['name'],
            'bio'=>$request['bio']            
        ]);
    return redirect('/cast');
    }

    public function index(){
        $cast = DB::table('cast')->get();
        return view ('show', compact('cast'));
    }

    public function show($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('tampil', compact('cast'));
    }

    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view('edit', compact('cast'));

    }

    public function update($id, Request $request){
        $request->validate([
            'name'=>'required',
            'bio'=>'required',

        ]);

        $upd = DB::table('cast')
        ->where('id', $id)
        ->update([
            'name'=> $request['name'],
            'bio'=> $request['bio']
        ]);

        return redirect('/cast');

    }

    public function destroy($id){
        DB::table('cast')->where('id', $id)->delete();
        return redirect('cast');
    }

}
